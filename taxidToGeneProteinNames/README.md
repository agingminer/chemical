# 遺伝子および蛋白質名取得ツール

NCBI API(e-utilities)を用いてtaxonomyIdから遺伝子名、あるいは蛋白質名を取得するPythonスクリプトです。


## 環境構築

下記のコマンドを実行するなどして必要なライブラリをインストールしてください。

```bash
Python2
pip install -r requirements.txt

Python3
pip3 install requests
```

# Python2 to Python3 convert
http://pythonconverter.com/

## 使い方

```py
Python2
python taxidToNames.py -t <taxId> -m {gene,protein} -o {tab,xml,json} -retry 3 > <taxId>.ext

python3 taxidToNames.py3 -t <taxId> -m {gene,protein} -o {tab,xml,json} -retry 3 > <taxId>.ext
```

  -m {protein,gene}  出力モードを蛋白質と遺伝子から選択します。

  -t T               taxonomyIdを指定します。

  -o {tab,xml,json}  出力形式を選択します。

  -retry             リクエストが失敗した場合に再度リクエストを送る回数を指定します。

## 動作確認済み環境
Python2
- Python 2.7.15rc1
- requests==2.21.0

Python3
- Python 3.7.4
- requests
```
$ pip3 install requests
Requirement already satisfied: requests in /usr/local/lib/python3.7/site-packages (2.23.0)
Requirement already satisfied: certifi>=2017.4.17 in /usr/local/lib/python3.7/site-packages (from requests) (2020.4.5.1)
Requirement already satisfied: urllib3!=1.25.0,!=1.25.1,<1.26,>=1.21.1 in /usr/local/lib/python3.7/site-packages (from requests) (1.25.9)
Requirement already satisfied: idna<3,>=2.5 in /usr/local/lib/python3.7/site-packages (from requests) (2.9)
Requirement already satisfied: chardet<4,>=3.0.2 in /usr/local/lib/python3.7/site-packages (from requests) (3.0.4)
```


