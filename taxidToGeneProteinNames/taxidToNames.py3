import argparse
import requests
import re
import sys
import random
from time import sleep


def Eutil(eutil, querystringData, retmode, n_retry):
    eutilBaseUrl = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils";
    delay = 1
    sleep(delay);

    retmodestr = '' if retmode == 'xml' else '&retmode=json'
    eutilUrl = "{}/{}.fcgi?{}{}".format(eutilBaseUrl, eutil, querystringData, retmodestr)
    if args.v:
        print(("eutilUrl", eutilUrl))
    
    while n_retry >= 0:
        try:
            res = requests.get(eutilUrl)
            if res.status_code == 200:
                if retmode == 'xml':
                    return res.text
                else:
                    return res.json()
            else:
                raise ValueError("eutil failed with status code %d for request: %s" % (res.status_code, querystringData))
        except Exception as e:
            n_retry -= 1
            rdelay = random.randint(20, 40)
            sleep(rdelay)
    print("*** Exception occured. Please try again later. ***", file=sys.stderr)
    # print("*** Exception occured. Please try again later. ***")
    sys.exit(1)


def process(args):

    count = re.compile('<Count>([\d]+)<\/Count>')
    idmat = re.compile('<Id>([\d]+)<\/Id>')
    max_i = 250
    start_i = 0
    total_i = start_i + 1
    haveTotal = False
    first = True

    while start_i < total_i:

        # Build querystring for esearch
        qs = "db={}&retstart={}&retmax={}&term={}[taxid]".format(args.m, start_i, max_i, str(args.t))
        #this option looks for GeneIDs with the taxId(args.t) value of interest that are 
        #NOT encoded on the mitochondrion and do not have RefSeqs of the type model.
        #Note use of the boolean NOT and field restriction
        #qs += "+NOT+source_mitochondrion[properties]+NOT+srcdb_refseq_model[properties]"

        esearch_result = Eutil("esearch", qs, 'json', args.retry)
        total_i = int(esearch_result['esearchresult']['count'])

        # Build querystring for esummary
        ids = ','.join(esearch_result['esearchresult']['idlist'])
        qs = "db={}&id={}".format(args.m, ids)

        esummary_result = Eutil ("esummary", qs, args.o, args.retry)
        
        if args.o == 'tab':
            if args.m == 'gene':
                cols = ['uid', 'name', 'description']
            if args.m == 'protein':
                cols = ['uid', 'title', 'extra']
            if first:
                print(('\t'.join(cols)))
            extractAndOutput(esummary_result, cols)
        if args.o == 'json':
            extractAndOutputJson(esummary_result)
        if args.o == 'xml':
            extractAndOutputXml(esummary_result, first);

        first = False
        
        start_i += max_i


def extractAndOutput(json, cols):

    result = json['result']
    for uid in result['uids']:
        print(('\t'.join(result[uid][col] for col in cols)))
    return

    if args.m == 'gene':
        docsum = re.compile('<DocumentSummary uid="([\d]+)">(.*?)<\/DocumentSummary>')
    if args.m == 'protein':
        docsum = re.compile('<DocumentSummary uid="([\d]+)">(.*?)<\/DocumentSummary>')
    rname = re.compile('<Name>(.+)<\/Name>')
    rdescription = re.compile('<Description>(.+)<\/Description>')

    xml = xml.replace('\t','').replace('\n','')
    m = docsum.search(xml)
    while m:
        id_ = m.group(1)
        match = m.group(2)
        m2 = rname.search(match)
        m3 = rdescription.search(match)
        name = None
        desc = None
        if m2: name = m2.group(1)
        if m3: desc = m3.group(1)
        print(("{}\t{}\t{}".format(id_, name, desc)))
        xml = xml[m.end()+1:]
        m = docsum.search(xml)


def extractAndOutputJson(json):
    result = json['result']
    print({uid: result[uid] for uid in result['uids']})


def extractAndOutputXml(xml, first):
    startTag = "<eSummaryResult>"
    stopTag = "</eSummaryResult>"
    start = 0 if first else xml.index(startTag) + len(startTag)
    stop = xml.index(stopTag)
    print((xml[start:stop]))


def main(args):
    # print(args)
    try:
        process(args)
    except Exception as e:
        pass

def check_non_negative(value):
    ivalue = int(value)
    if ivalue < 0:
        raise argparse.ArgumentTypeError("%s is an invalid non-negative int value" % value)
    return ivalue

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', type=str,
                        choices=['protein', 'gene'],
                        required=True,
                        help='mode',)
    parser.add_argument('-t', type=int,
                        required=True,
                        help='taxonomyId',)
    parser.add_argument('-retry', type=check_non_negative,
                        default=0,
                        help='the number of retry request',)
    parser.add_argument('-o', type=str,
                        choices=['xml', 'json', 'tab'],
                        required=True,
                        help='output options',)
    parser.add_argument('-v', action='store_true',
                        help='verbose',)
    args = parser.parse_args()
    main(args)
