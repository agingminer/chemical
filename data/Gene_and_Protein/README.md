# Protein and Gene names

# Update Date: 2020-04-22 (JST)

## Tools

* https://gitlab.com/agingminer/chemical/-/tree/master/taxidToGeneProteinNames


| taxId | scientificName | commonName |
| ------ | ------ |  ------ | 
| 9606 | Homo sapiens | human |
| 10116 | Rattus norvegicus | Norway rat |
| 9606 | Mus musculus | house mouse |


## file name: {taxId}-{gene or protein}.{tsv or json}


```
python taxidToNames.py -t 9606 -m gene -o json -retry 3 > 9606-gene.json
python taxidToNames.py -t 9606 -m protein -o json -retry 3 > 9606-protein.json
python taxidToNames.py -t 10116 -m gene -o json -retry 3 > 10116-gene.json
python taxidToNames.py -t 10116 -m protein -o json -retry 3 > 10116-protein.json
python taxidToNames.py -t 10090 -m gene -o json -retry 3 > 10090-gene.json
python taxidToNames.py -t 10090 -m protein -o json -retry 3 > 10090-protein.json

python taxidToNames.py -t 9606 -m gene -o tab -retry 3 > 9606-gene.tsv
python taxidToNames.py -t 9606 -m protein -o tab -retry 3 > 9606-protein.tsv
python taxidToNames.py -t 10116 -m gene -o tab -retry 3 > 10116-gene.tsv
python taxidToNames.py -t 10116 -m protein -o tab -retry 3 > 10116-protein.tsv
python taxidToNames.py -t 10090 -m gene -o tab -retry 3 > 10090-gene.tsv
python taxidToNames.py -t 10090 -m protein -o tab -retry 3 > 10090-protein.tsv

```


